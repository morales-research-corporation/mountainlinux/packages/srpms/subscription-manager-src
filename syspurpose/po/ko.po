# Ondřej Januš <ojanus@redhat.com>, 2019. #zanata
# Ludek Janda <ljanda@redhat.com>, 2020. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-29 13:05-0400\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-07-14 08:05-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Korean\n"
"Language: ko\n"
"X-Generator: Zanata 4.6.2\n"
"Plural-Forms: nplurals=1; plural=0\n"

#: src/syspurpose/cli.py:29
#, python-brace-format
msgid ""
"Warning: A {attr} of \"{download_value}\" was recently set for this system "
"by the entitlement server administrator.\n"
"{advice}"
msgstr ""
"경고 : 최근 인타이틀먼트 서버 관리자가 \"{download_value}\"의 {attr}를 이 시스템에 설정했습니다.\n"
"{advice}"

#: src/syspurpose/cli.py:31
#, python-brace-format
msgid ""
"If you'd like to overwrite the server side change please run: {command}"
msgstr "서버 측 변경 사항을 덮어 쓰기하려면 다음을 실행하십시오. {command}"

#: src/syspurpose/cli.py:47
#, python-brace-format
msgid "Added {value} to {prop_name}."
msgstr "{value} 값을 {prop_name}에 추가했습니다."

#: src/syspurpose/cli.py:50
#, python-brace-format
msgid "Not adding value {value} to {prop_name}; it already exists."
msgstr "{value} 값을 {prop_name}에 추가하지 않습니다; 이미 존재합니다."

#: src/syspurpose/cli.py:56 src/syspurpose/cli.py:85
#, python-brace-format
msgid "{attr} updated."
msgstr "{attr}이/가 업데이트되었습니다."

#: src/syspurpose/cli.py:79
#, python-brace-format
msgid "Removed \"{val}\" from {name}."
msgstr "{name}에서 \"{val}\"을/를 제거했습니다."

#: src/syspurpose/cli.py:81
#, python-brace-format
msgid "Not removing value \"{val}\" from {name}; it was not there."
msgstr "{name}에서 \"{val}\" 값을 제거하지 않습니다. 값이 존재하지 않습니다."

#: src/syspurpose/cli.py:108
#, python-brace-format
msgid "{attr} set to \"{val}\"."
msgstr "{attr}이/가 \"{val}로 설정되었습니다."

#: src/syspurpose/cli.py:129
#, python-brace-format
msgid "{attr} unset."
msgstr "{attr} 설정을 해제합니다."

#: src/syspurpose/cli.py:181
msgid "Sets the value for the given property"
msgstr "주어진 속성에 대한 값 설정"

#: src/syspurpose/cli.py:187 src/syspurpose/cli.py:209
msgid "The name of the property to set/update"
msgstr "설정/업데이트할 속성 이름"

#: src/syspurpose/cli.py:193
msgid "The value to set"
msgstr "설정할 값"

#: src/syspurpose/cli.py:202
msgid "Unsets (clears) the value for the given property"
msgstr "주어진 속성에 대한 설정 취소 (설정 지우기)"

#: src/syspurpose/cli.py:216
msgid "Adds the value(s) to the given property"
msgstr "주어진 속성에 대한 값 추가"

#: src/syspurpose/cli.py:222 src/syspurpose/cli.py:244
msgid "The name of the property to update"
msgstr "업데이트할 속성 이름"

#: src/syspurpose/cli.py:228
msgid "The value(s) to add"
msgstr "추가할 값"

#: src/syspurpose/cli.py:238
msgid "Removes the value(s) from the given property"
msgstr "주어진 속성에서 값을 제거합니다"

#: src/syspurpose/cli.py:250
msgid "The value(s) to remove"
msgstr "삭제할 값"

#: src/syspurpose/cli.py:260
msgid "Set the system role to the system syspurpose"
msgstr "시스템 역할을 시스템 syspurpose로 설정"

#: src/syspurpose/cli.py:268
msgid "Clear set role"
msgstr "역할 설정 삭제"

#: src/syspurpose/cli.py:276
msgid "Add addons to the system syspurpose"
msgstr "시스템 syspurpose에 애드온 추가"

#: src/syspurpose/cli.py:284
msgid "Remove addons from the system syspurpose"
msgstr "시스템 syspurpose에서 애드온 제거"

#: src/syspurpose/cli.py:291
msgid "Clear set addons"
msgstr "애드온 설정 삭제"

#: src/syspurpose/cli.py:299
msgid "Set the system sla"
msgstr "시스템의 SLA 설정"

#: src/syspurpose/cli.py:306
msgid "Clear set sla"
msgstr "SLA 설정 해제"

#: src/syspurpose/cli.py:314
msgid "Set the system usage"
msgstr "시스템의 사용 방법 설정"

#: src/syspurpose/cli.py:322
msgid "Clear set usage"
msgstr "사용 방법 설정 삭제"

#: src/syspurpose/cli.py:330
msgid "Show the current system syspurpose"
msgstr "현재 시스템의 syspurpose 표시"

#: src/syspurpose/cli.py:348
msgid ""
"WARNING: Setting syspurpose in containers has no effect.Please run "
"syspurpose on the host.\n"
msgstr "경고: 컨테이너에 syspurpose를 설정해도 작동하지 않습니다. 호스트에서 syspurpose를 실행하십시오.\n"

#: src/syspurpose/cli.py:360
msgid ""
"Warning: Unable to sync system purpose with subscription management server: "
"subscription_manager module is not available."
msgstr ""
"경고: 서브스크립션 관리 서버와 syspurpose를 동기화할 수 없습니다 : subscription_manager 모듈을 사용할 수 "
"없습니다."

#: src/syspurpose/files.py:88
msgid "Error: Malformed data in file {}; please review and correct."
msgstr "오류: 파일 {}의 데이터 형식이 잘못되었습니다; 확인 후 수정하십시오."

#: src/syspurpose/files.py:94
msgid "Cannot read syspurpose file {}\n"
"Are you root?"
msgstr "syspurpose 파일 {}을/를 읽을 수 없습니다.\n"
"root 사용자입니까?"

#: src/syspurpose/main.py:32
msgid "User interrupted process"
msgstr "사용자 중단 프로세스"

#: src/syspurpose/utils.py:59
msgid "Cannot create directory {}\n"
"Are you root?"
msgstr "디렉토리 {}을/를 만들 수 없습니다.\n"
"root 사용자입니까?"

#: src/syspurpose/utils.py:80
msgid "Cannot create file {}\n"
"Are you root?"
msgstr "파일 {}을/를 만들 수 없습니다.\n"
"root 사용자입니까?"
